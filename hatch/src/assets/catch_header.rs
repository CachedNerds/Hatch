pub struct CatchHeader;

impl CatchHeader {
  pub fn new() -> CatchHeader {
    CatchHeader
  }

  pub fn name() -> String {
    String::from("catch.hpp")
  }
}
